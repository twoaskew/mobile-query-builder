<?php

use MobileDe\Query\CarQueryBuilder;
use Carbon\Carbon;

class CarQueryBuilderTest extends PHPUnit_Framework_TestCase
{
	protected $baseurl;

	protected $queryBuilder;

	public function setup()
	{
		$this->baseUrl = 'https://services.mobile.de/search-api/search?classification=refdata/classes/Car&';
		$this->queryBuilder = new CarQueryBuilder;
	}

	/** @test */
	public function it_fetches_100_listings_per_api_request_by_default() {
		$request = $this->queryBuilder->get();

		$this->assertEquals($this->baseUrl . 'page.size=100', $request);
	}

	/** @test */
	public function it_can_get_ads_with_images() {
		$request = $this->queryBuilder->withImages()->get();

		$this->assertEquals($this->baseUrl . 'imageCount.min=1&page.size=100', $request);
	}

	/** @test */
	public function it_can_fetch_any_number_of_cars() {
		$request = $this->queryBuilder->pageSize(40)->get();

		$this->assertEquals($this->baseUrl . 'page.size=40', $request);
	}

	/** @test */
	public function it_accepts_string_as_a_number_of_pages() {
		$request = $this->queryBuilder->pageSize('96')->get();

		$this->assertEquals($this->baseUrl . 'page.size=96', $request);
	}

	/** @test */
	public function it_has_a_maximum_page_size() {
		$request = $this->queryBuilder->pageSize(12342)->get();

		$this->assertEquals($this->baseUrl . 'page.size=100', $request);
	}

	/** @test */
	public function it_can_get_cars_above_certain_price() {
		$request = $this->queryBuilder->minPrice(2500)->get();

		$this->assertEquals($this->baseUrl . 'price.min=2500&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_cars_below_specified_price() {
		$request = $this->queryBuilder->maxPrice(10000)->get();

		$this->assertEquals($this->baseUrl. 'price.max=10000&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_cars_in_price_range() {
		$request = $this->queryBuilder->price(2500, 3000)->get();

		$this->assertEquals($this->baseUrl . 'price.min=2500&price.max=3000&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_ads_from_a_specific_country() {
		$request = $this->queryBuilder->from('DE')->get();

		$this->assertEquals($this->baseUrl . 'country=DE&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_ads_from_many_countries() {
		$request = $this->queryBuilder->from(['DE', 'AT', 'LV'])->get();

		$this->assertEquals($this->baseUrl . 'country=AT,DE,LV&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_a_specified_page() {
		$request = $this->queryBuilder->price(2500, 10000)->pageNumber(2)->get();
		
		$this->assertEquals($this->baseUrl . 'price.min=2500&price.max=10000&page.number=2&page.size=100', $request);
	}

	/** @test */
	public function it_throws_an_exception_when_bad_country_code_is_provided() {
		$this->expectException('Exception');
		$this->expectExceptionMessage('Invalid country code');
		$request = $this->queryBuilder->from('LATVI')->get();
	}

	/** @test */
	public function it_throws_an_exception_when_a_bad_country_code_is_mixed_with_valid_ones() {
		$this->expectException('Exception');
		$this->expectExceptionMessage('Invalid country code');
		$request = $this->queryBuilder->from(['AT', 'DE', 'LAT']);
	}

	/** @test */
	public function it_throws_an_excepton_if_no_countries_are_provided() {
		$this->expectException('Exception');
		$this->expectExceptionMessage('Empty country code');
		$request = $this->queryBuilder->from('');
	}

	/** @test */
	public function it_throws_an_exception_if_an_empty_array_of_countries_is_provided() {
		$this->expectException('Exception');
		$this->expectExceptionMessage('Empty country code');
		$request = $this->queryBuilder->from([]);
	}

	/** @test */
	public function it_can_get_ads_sorted_by_date_created() {
		$request = $this->queryBuilder->sortBy('created', 'ASC')->get();
		$request2 = $this->queryBuilder->sortBy('created', 'DESC')->get();
		
		$this->assertEquals($this->baseUrl . 'sort.field=creationTime&sort.order=ASCENDING&page.size=100', $request);
		$this->assertEquals($this->baseUrl . 'sort.field=creationTime&sort.order=DESCENDING&page.size=100', $request2);	
	}

	/** @test */
	public function it_can_get_ads_sorted_by_date_modified() {
		$request = $this->queryBuilder->sortBy('modified', 'ASC')->get();
		$request2 = $this->queryBuilder->sortBy('modified', 'DESC')->get();
		
		$this->assertEquals($this->baseUrl . 'sort.field=modificationTime&sort.order=ASCENDING&page.size=100', $request);
		$this->assertEquals($this->baseUrl . 'sort.field=modificationTime&sort.order=DESCENDING&page.size=100', $request2);	
	}

	/** @test */
	public function it_throws_an_exception_if_invalid_order_is_provided() {
		$this->expectException('Exception');
		$this->expectExceptionMessage('Invalid order');
		$request = $this->queryBuilder->sortBy('created', 'SDF')->get();
	}

	/** @test */
	public function it_throws_an_exception_if_invalid_() {
		$this->expectException('Exception');
		$this->expectExceptionMessage('Invalid orderBy field');

		$request = $this->queryBuilder->sortBy('updated', 'ASC')->get();	
	}

	/** @test */
	public function it_can_exclude_damaged_vehicles_from_search() {
		$request = $this->queryBuilder->damaged(false)->get();

		$this->assertEquals($this->baseUrl . 'damageUnrepaired=0&page.size=100', $request);
	}

	/** @test */
	public function it_can_include_damaged_vehicles_in_search() {
		$request = $this->queryBuilder->damaged(true)->get();

		$this->assertEquals($this->baseUrl . 'damageUnrepaired=1&page.size=100', $request);
	}

	/** @test */
	public function it_can_exclude_accident_damaged_vehicles_from_search() {
		$request = $this->queryBuilder->accidentDamaged(false)->get();

		$this->assertEquals($this->baseUrl. 'accidentDamaged=0&page.size=100', $request);
	}

	/** @test */
	public function it_can_include_accident_damaged_vehicles_in_search() {
		$request = $this->queryBuilder->accidentDamaged(true)->get();

		$this->assertEquals($this->baseUrl. 'accidentDamaged=1&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_last_days_ads() {
		$request = $this->queryBuilder->latest()->get();
		$date = str_replace('+', '%2B', Carbon::today('Europe/Berlin')->subDays(1)->toRfc3339String());
		$this->assertEquals($this->baseUrl . 'modificationTime.min='. $date .'&page.size=100', $request);
	}

	/** @test */
	public function it_can_get_a_number_of_specified_days_old_ads() {
		$request = $this->queryBuilder->latest(5)->get();
		$date = str_replace('+', '%2B', Carbon::today('Europe/Berlin')->subDays(5)->toRfc3339String());
		$this->assertEquals($this->baseUrl . 'modificationTime.min='. $date .'&page.size=100', $request);
	}
	
}