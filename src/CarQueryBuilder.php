<?php

namespace MobileDe\Query;

class CarQueryBuilder extends QueryBuilder {

	protected function getBaseUrl()
	{
		return 'https://services.mobile.de/search-api/search?classification=refdata/classes/Car';
	}

}