<?php

namespace MobileDe\Query;

use \Exception;
use Carbon\Carbon;

abstract class QueryBuilder {

	protected $queryKeys = [];

	protected $query = '';

	// Each implementation of this class needs a base url
	abstract protected function getBaseUrl();

	public function damaged($include)
	{
		if($this->queryHasKey('damaged'))
		{
			return $this;
		}
		$value = 0;
		if($include)
		{
			$value = 1;
		}
		$this->addToQuery('damageUnrepaired=' . $value);
		$this->addQueryKey('damaged');
		return $this;
	}

	public function accidentDamaged($include)
	{
		if($this->queryHasKey('accident'))
		{
			return $this;
		}
		$value = 0;
		if($include)
		{
			$value = 1;
		}
		$this->addToQuery('accidentDamaged=' . $value);
		$this->addQueryKey('accident');
		return $this;
	}

	public function withImages()
	{
		if ($this->queryHasKey('withImages'))
		{
			return $this;
		}
		$this->addToQuery('imageCount.min=1');
		$this->addQueryKey('withImages');
		return $this;
	}

	public function from($countries)
	{
		if ($this->queryHasKey('countries'))
		{
			return $this;
		}
		$this->checkIfCountriesAreValid($countries);
		$this->addCountries($countries);
		return $this;
	}

	protected function addCountries($countries)
	{
		if(is_array($countries)) {
			sort($countries);
			$countries = implode(",", $countries);
		}
		$this->addToQuery('country=' . $countries);
		$this->addQueryKey('countries');
	}



	protected function checkIfCountriesAreValid($countries)
	{
		if(empty($countries))
		{
			throw new Exception('Empty country code');
		}
		$pattern = "/^[A-Z]{2}$/";
		if(is_array($countries)) {
			foreach($countries as $country) {
				if (! preg_match_all($pattern, $country))
				{
					throw new Exception('Invalid country code');
				}
			}
			return true;
		}
		if (! preg_match_all($pattern, $countries)) {
			throw new Exception('Invalid country code');
		}
		return true;
	}

	public function sortBy(string $field, string $order)
	{
		if ($this->queryHasKey('sort'))
		{
			return $this;
		}
		$field = $this->transformSortField($field);
		$order = $this->transformSortOrder($order);
		$this->addToQuery('sort.field='. $field);
		$this->addToQuery('sort.order='. $order);
		$this->addQueryKey('sort');
		return $this;
	}

	protected function transformSortOrder($order)
	{	
		if ($order == 'ASC') {
			return 'ASCENDING';
		}
		if ($order == 'DESC') {
			return 'DESCENDING';
		}
		throw new Exception('Invalid order');
	}

	protected function transformSortField($field)
	{
		if ($field == 'created') {
			return  'creationTime';
		}
		if ($field == 'modified') {
			return  'modificationTime';
		}
		throw new Exception('Invalid orderBy field');
	}

	public function pageSize(int $size = 100)
	{
		if($this->queryHasKey('pageSize'))
		{
			return $this;
		}
		if($size > 100) {
			$size = 100;
		}
		if($size < 1) {
			$size = 1;
		}
		$this->addToQuery('page.size=' . $size);
		$this->addQueryKey('pageSize');
		return $this;
	}

	public function minPrice(int $price)
	{
		if($this->queryHasKey('minPrice'))
		{
			return $this;
		}
		$this->addToQuery('price.min=' . $price);
		$this->addQueryKey('minPrice');
		return $this;
	}

	public function maxPrice(int $price)
	{
		if($this->queryHasKey('maxPrice'))
		{
			return $this;
		}
		$this->addToQuery('price.max=' . $price);
		$this->addQueryKey('maxPrice');
		return $this;
	}

	public function pageNumber(int $page = null)
	{
		if($this->queryHasKey('pageNumber') || $page == null)
		{
			return $this;
		}
		$this->addToQuery('page.number=' . $page);
		$this->addQueryKey('pageNumber');
		return $this;
	}

	public function price(int $minPrice, int $maxPrice)
	{
		if ($minPrice < $maxPrice)
		{
			return $this->minPrice($minPrice)->maxPrice($maxPrice);
		}
		return $this;
	}

	protected function buildDefaultQuery()
	{
		$this->pageSize();
	}

	public function get()
	{
		$this->buildDefaultQuery();
		$query = $this->query;
		$this->cleanUp();
		return $this->getBaseUrl() . $query;
	}

	protected function cleanUp()
	{
		$this->query = '';
		$this->queryKeys = [];
	}

	protected function addToQuery($query)
	{
		$this->query .= '&' . $query;
	}
	
	/**
	 * Add $key to the queryKeys array
	 * @param string $key query parameter key
	 * 
	 */
	protected function addQueryKey(string $key)
	{
		array_push($this->queryKeys, $key);
	}

	/**
	 * Check if $key is present in the queryKeys array
	 * @param  string $key key of the query parameter
	 * @return bool      
	 */
	protected function queryHasKey(string $key)
	{
		return in_array($key, $this->queryKeys);
	}

	public function latest($days = 1)
	{
		if($this->queryHasKey('latest'))
		{
			return $this;
		}
		// get date
		$date = $this->getDateString($days);
		$this->addToQuery('modificationTime.min=' . $date);
		$this->addQueryKey('latest');
		return $this;
	}

	protected function getDateString($days)
	{
		$date = Carbon::today('Europe/Berlin')->subDays($days)->toRfc3339String();
		return $this->encodeDateForMobile($date);
	}

	protected function encodeDateForMobile($string)
	{
		return str_replace('+', '%2B', $string);
	}
	
}